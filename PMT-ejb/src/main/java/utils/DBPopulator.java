package utils;

import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import interfaces.CategoryServiceLocal;
import interfaces.ProjectServiceLocal;
import interfaces.TaskServiceLocal;
import tn.esprit.persistance.Category;
import tn.esprit.persistance.Project;
import tn.esprit.persistance.State;
import tn.esprit.persistance.Task;
import tn.esprit.persistance.TaskPk;
import tn.esprit.persistance.TeamMember;

@Singleton
@Startup
public class DBPopulator {

	@EJB
	private TaskServiceLocal EjbTaskServiceLocal;
	
	@EJB
	private ProjectServiceLocal EjbProjectServiceLocal;
	
	@EJB
	private CategoryServiceLocal EjbCategoryServiceLocal;
	

	public DBPopulator() {
		
	}
	
	@PostConstruct
	public void createTasks(){
		
		List<Task> ListOfAllTasks;
		List<Task>ListOfDoneTasks;
		
	/*	Category category = new Category("Java Entreprise Edition");
		EjbCategoryServiceLocal.add(category);
		Category category2 = new Category("Angular Js");
		EjbCategoryServiceLocal.add(category2);*/
		/*Calendar calendarStart = Calendar.getInstance();
		Calendar calendarEndDate = Calendar.getInstance();
		calendarStart.set(2016,11,1);
		calendarEndDate.set(2016,12,30);
		
		
		Project project1 = new Project(1,"prject management tool","sdqsd","azertyy","sqdqsdqsd",calendarStart.getTime(),calendarEndDate.getTime(),category);
		EjbProjectServiceLocal.add(project1);*/
		
		/*TeamMember Member1 = new TeamMember(1,22,"Ahmed Antar","sjdsqdkjsqkj","ahmed.antar@esprit.tn");
		TeamMember Member2 = new TeamMember(2,25,"Ghada","sjdsqdkjsqkj","Ghada@esprit.tn");
		
		Calendar calendarDeadline = Calendar.getInstance();
		Calendar calendarStartDate = Calendar.getInstance();
		calendarDeadline.set(2016,12,30);
		calendarStartDate.set(2016,12,1);
		
	
	     TaskPk PrimaryKeyTask1 = new TaskPk(Member1.getIdTeamMember(),1,"jkhjkh");
	     TaskPk PrimaryKeyTask2 = new TaskPk(Member2.getIdTeamMember(),1,"ggjggh");
	     
	     Task taskToAdd1 = new Task(PrimaryKeyTask1,State.ToDo,calendarDeadline.getTime(),calendarStartDate.getTime());
	     Task taskToAdd2 = new Task(PrimaryKeyTask2,State.ToDo,calendarDeadline.getTime(),calendarStartDate.getTime());
	     
	     EjbTaskServiceLocal.addTask(taskToAdd1);
	     EjbTaskServiceLocal.addTask(taskToAdd2);*/
		
		 TaskPk PkTask = new TaskPk(3,1,"ahmedTask");
		 TaskPk PkTask2 = new TaskPk(3,1,"ahmedTask2");
		 
		 Calendar calendarDeadline1 = Calendar.getInstance();
		 Calendar calendarStartDate1 = Calendar.getInstance();
		 calendarDeadline1.set(2016,12,30);
		 calendarStartDate1.set(2016,11,2);
		 
		 TaskPk PkTaskValidation1 = new TaskPk(77,77,"ahmedTaskValidation");
		 TaskPk PkTaskValidation2 = new TaskPk(77,77,"ahmedTaskValidation2");
		 
		 Task TaskValidation1 = new Task(PkTaskValidation1,State.ToDo,calendarDeadline1.getTime(),calendarStartDate1.getTime());
		 //EjbTaskServiceLocal.addTask(TaskValidation1);
		 
		 Task TaskValidation2 = new Task(PkTaskValidation2,State.Done,calendarDeadline1.getTime(),calendarStartDate1.getTime());
		 //EjbTaskServiceLocal.addTask(TaskValidation2);
		 //EjbTaskServiceLocal.MarkTaskAsDone(TaskValidation2);
		 
		 //EjbTaskServiceLocal.deleteTaskById(PkTaskValidation1);
		 //EjbTaskServiceLocal.deleteTaskById(PkTaskValidation2);
		 
		 
	     ListOfAllTasks = EjbTaskServiceLocal.findAllTasks();
	     ListOfDoneTasks = EjbTaskServiceLocal.AcomplishTasksPerProject(PkTask);
	     System.out.println("***************ALL TASKS***************");
	     for(Task task:ListOfAllTasks){
	    	 System.out.println(task.toString());
	    	 
	     }
	     System.out.println("***************DONE TASKS***************");
	     for(Task task:ListOfDoneTasks){
	    	 System.out.println(task.toString());
	    	 
	     }
	     
	     System.out.println("***************Project Percentage Of Completion***************");
	     System.out.println(EjbTaskServiceLocal.ProjectPercentageOfCompletion(PkTask.getIdProject()));
	     
	     System.out.println("***************Task Deadline Verification***************");
	     System.out.println(EjbTaskServiceLocal.testStatutOfTask(PkTask));
	     
	     //Test service
	     
	    /* System.out.println("***************Task Deadline Verification Validation***************");
	     System.out.println(EjbTaskServiceLocal.testStatutOfTask(PkTaskValidation1));
	     
	     System.out.println("***************Project Percentage Of Completion Validation***************");
	     System.out.println(EjbTaskServiceLocal.ProjectPercentageOfCompletion(PkTaskValidation1.getIdProject()));*/
	}
	
	

}
