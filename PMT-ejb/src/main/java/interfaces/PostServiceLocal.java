package interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Post;

@Local
public interface PostServiceLocal {
	
void add(Post post);
	
	void delete(Post post);
	
	void deleteById(int id);
	
	void update(Post post);
	
	List<Post> findPostsByContent(String Content );
	
	List<Post> findAllPosts();
	
	List<Post> findPostById(int IdPost);

}
