package interfaces;


import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Category;

@Local

public interface CategoryServiceLocal {
	
    void add(Category category);
	
	void delete(Category category);
	
	void deleteById(int id);
	
	void update(Category category);
	
	List<Category> findCategoryByName(String Name );
	
	List<Category> findAllCategories();
	
	List<Category> findCategoryById(int IdCategory);

}
