package interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Topic;

@Local
public interface TopicServiceLocal {
	
void add(Topic topic);
	
	void delete(Topic topic);
	
	void deleteById(int id);
	
	void update(Topic topic);
	
	List<Topic> findTopicsByTitle(String Title );
	
	List<Topic> findAllTopics();
	
	List<Topic> findTopicById(int IdTopic);

}
