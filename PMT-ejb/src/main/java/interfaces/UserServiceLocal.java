package interfaces;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.User;

@Local
public interface UserServiceLocal {



	void deleteUser(int id);

	User findUserById(int id);

	void updateUser(User user);

	User findUserByEmail(String email);

    List<User> find(); 
   
	public User findusersByName(String name);

	User findUserByState(int state);

	User authentificationUser(String login, String password);

	User authentificationManager(String login, String password);

	User verifyState(int state);

	List<User> findState();

	void validateUser(User user);

	//ArrayList<User> getListAccountActivate(int value);

	//ArrayList<User> getListAccountDeactivate(int value);

	//Long getNumberUsersnoconfirmed(int value);

	//ArrayList<User> getConfirmedUsers();

	boolean addUser(User user);

	void desactivateUser(User user);

	List<User> findUservalidate();

	List<User> findUsernovalidate();
	



}
