package interfaces;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Project;

@Local

public interface ProjectServiceLocal {
	
	void add(Project project);
	void delete(Project project);
	void deleteById(int id);
	void update(Project project);
	Project findProjectById(int id);
	List<Project>  findProjectByName(String name);
	List<Project> findAllProjects();

}
