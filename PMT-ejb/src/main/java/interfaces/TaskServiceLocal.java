package interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Task;
import tn.esprit.persistance.TaskPk;

@Local
public interface TaskServiceLocal {
	void addTask(Task taskToAdd);
	void deleteTask(Task taskToDelete);
	void deleteTaskById(TaskPk PkTaskToDelete);
	void updateTask(Task taskToUpdate);
	Task findTaskById(TaskPk PkTaskToFind);
	List<Task> findAllTasks();
	List<Task> AcomplishTasksPerProject(TaskPk PkTaskToShowAccomplishTasks);
	List<Task> NotAcomplishTasksPerProject( TaskPk PkTaskToShowNotAccomplishTasks);
	String ProjectPercentageOfCompletion(int idProject);
	String testStatutOfTask(TaskPk PkTaskToShowAccomplishTasks );
	void MarkTaskAsDone(Task taskToUpdate);
	void MarkTaskAsDoing(Task taskToUpdate);

}
