package interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.persistance.Client;


@Local
public interface ClientServiceLocal {
		public List<Client> GetAllClient();
		public void UpdateClient(Client client);
		public void DeleteClient(Client client);
		public void AddClient(Client client);
}
