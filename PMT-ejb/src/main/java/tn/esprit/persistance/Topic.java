package tn.esprit.persistance;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Topic
 *
 */
@Entity
@Table(name="Topic")

public class Topic implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IdTopic;
	private String Title;
	private Date CreationDate;
	private Date ReplyDate;
	private int Views;
	private int LastUser;
	private int Solved = 0;
	
	@ManyToOne
	private User Creator ;
	
	private static final long serialVersionUID = 1L;

	public Topic() {
		super();
	}   
	public int getIdTopic() {
		return this.IdTopic;
	}

	public void setIdTopic(int IdTopic) {
		this.IdTopic = IdTopic;
	}   
	public String getTitle() {
		return this.Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}   
	public Date getCreationDate() {
		return this.CreationDate;
	}

	public void setCreationDate(Date CreationDate) {
		this.CreationDate = CreationDate;
	}   
	public Date getReplyDate() {
		return this.ReplyDate;
	}

	public void setReplyDate(Date ReplyDate) {
		this.ReplyDate = ReplyDate;
	}   
	public int getViews() {
		return this.Views;
	}

	public void setViews(int Views) {
		this.Views = Views;
	}   
 
	public int getLastUser() {
		return this.LastUser;
	}

	public void setLastUser(int LastUser) {
		this.LastUser = LastUser;
	}
	public int getSolved() {
		return this.Solved;
	}
	public void setSolved(int solved) {
		this.Solved = solved;
	}
   
}

