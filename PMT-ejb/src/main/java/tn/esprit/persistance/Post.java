package tn.esprit.persistance;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Post
 *
 */
@Entity
@Table(name="Post")

public class Post implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IdPost;
	private String Content;
	private Date PostDate;
	
	@ManyToOne
	private Topic topic ;
	@ManyToOne
	private User Creator ;
	private static final long serialVersionUID = 1L;

	public Post() {
		super();
	}   
	public int getIdPost() {
		return this.IdPost;
	}

	public void setIdPost(int IdPost) {
		this.IdPost = IdPost;
	}   

	public String getContent() {
		return this.Content;
	}

	public void setContent(String Content) {
		this.Content = Content;
	}   
	public Date getPostDate() {
		return this.PostDate;
	}

	public void setPostDate(Date PostDate) {
		this.PostDate = PostDate;
	}   

   
}
