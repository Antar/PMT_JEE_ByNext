package tn.esprit.persistance;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Project
 *
 */
@Entity()
@Table(name="Project")

public class Project implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IdProject;
	private String Name;
	private String Description;
	private String Type;
	private String Priority;
	@Temporal(TemporalType.DATE)
	private Date StartDate;
	@Temporal(TemporalType.DATE)
	private Date EndDate;
	@JsonBackReference
	@OneToMany(fetch=FetchType.EAGER,mappedBy="project")
	private List<Task> tasks;
	@JsonManagedReference
	@ManyToOne
	private Category Category ;
	
	//@OneToOne(mappedBy="project")
	//private Team team; //master
	private static final long serialVersionUID = 1L;

	public Project() {
		super();
	}   
	
	
		
	public Project(int idProject, String name, String description, String type, String priority, Date startDate,
			Date endDate, Category category) {
		IdProject = idProject;
		Name = name;
		Description = description;
		Type = type;
		Priority = priority;
		StartDate = startDate;
		EndDate = endDate;
		Category=category;
	}



	public int getIdProject() {
		return this.IdProject;
	}

	public void setIdProject(int IdProject) {
		this.IdProject = IdProject;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}   
	public String getType() {
		return this.Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}   
	public String getPriority() {
		return this.Priority;
	}

	public void setPriority(String Priority) {
		this.Priority = Priority;
	}   
	public Date getStartDate() {
		return this.StartDate;
	}

	public void setStartDate(Date StartDate) {
		this.StartDate = StartDate;
	}   
	public Date getEndDate() {
		return this.EndDate;
	}

	public void setEndDate(Date EndDate) {
		this.EndDate = EndDate;
	}
	
	
	public List<Task> GetTasks() {
		return tasks;
	}
	public void SetTasks(List<Task> tasks) {
		this.tasks = tasks;
	}



	public Category getCategory() {
		return Category;
	}



	public void setCategory(Category category) {
		Category = category;
	}
	
	
	
   
}
