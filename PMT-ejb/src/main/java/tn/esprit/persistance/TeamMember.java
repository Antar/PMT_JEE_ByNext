package tn.esprit.persistance;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity()
@Table(name="TeamMember")

public class TeamMember implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTeamMember;
	private int idTeam;
	private String name;
	private String review;
	private String email;
	@JsonBackReference
	@OneToMany(fetch=FetchType.EAGER,mappedBy="teamMember")
	private List<Task> tasks;
	//@ManyToOne(fetch = FetchType.EAGER)
	//private Team team;
	
private static final long serialVersionUID = 1L;

	 public TeamMember() {
		// TODO Auto-generated constructor stub
		super();
	}
	 
	 

	public TeamMember(int idTeamMember, int idTeam, String name, String review, String email) {
		
		this.idTeamMember = idTeamMember;
		this.idTeam = idTeam;
		this.name = name;
		this.review = review;
		this.email = email;
	}



	public int getIdTeamMember() {
		return idTeamMember;
	}

	public void setIdTeamMember(int idTeamMember) {
		this.idTeamMember = idTeamMember;
	}

	public int getIdTeam() {
		return idTeam;
	}

	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}  
	
	 
	 
	

}
