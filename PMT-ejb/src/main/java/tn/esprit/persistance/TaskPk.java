package tn.esprit.persistance;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TaskPk implements Serializable{
	
	private int idTeamMember;
	private int idProject;
	private String Description;

	/**
	 * 
	 */
	private static final long serialVersionUID = 3L;
	
	public TaskPk(){
		
	}
	
	public TaskPk(int idTeamMember, int idProject, String Description){
		this.idTeamMember=idTeamMember;
		this.idProject=idProject;
		this.Description=Description;
	}

	public int getIdTeamMember() {
		return idTeamMember;
	}

	public void setIdTeamMember(int idTeamMember) {
		this.idTeamMember = idTeamMember;
	}

	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	

	public String GetDescription() {
		return Description;
	}

	public void SetDescription(String description) {
		Description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Description == null) ? 0 : Description.hashCode());
		result = prime * result + idProject;
		result = prime * result + idTeamMember;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskPk other = (TaskPk) obj;
		if (Description == null) {
			if (other.Description != null)
				return false;
		} else if (!Description.equals(other.Description))
			return false;
		if (idProject != other.idProject)
			return false;
		if (idTeamMember != other.idTeamMember)
			return false;
		return true;
	}

	

}
