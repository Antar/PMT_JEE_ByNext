package tn.esprit.persistance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Team implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 789L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_team;
	private String nom_team;
	private String projectName;
	private Date deliveryDate;
	private int score;
	private String teamStatus;
	
	/*@OneToOne
	private Project project;
	@OneToMany(fetch = FetchType.EAGER)
	private List<TeamMember> members;*/

	public Team() {
		super();

	}

	
	public Team(int id_team, String nom_team, String projectName, int score, String teamStatus) {
		super();
		this.id_team = id_team;
		this.nom_team = nom_team;
		this.projectName = projectName;
		this.score = score;
		this.teamStatus = teamStatus;
	}


	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public int getId_team() {
		return id_team;
	}

	public void setId_team(int id_team) {
		this.id_team = id_team;
	}

	public String getNom_team() {
		return nom_team;
	}

	public void setNom_team(String nom_team) {
		this.nom_team = nom_team;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getTeamStatus() {
		return teamStatus;
	}

	public void setTeamStatus(String teamStatus) {
		this.teamStatus = teamStatus;
	}

}
