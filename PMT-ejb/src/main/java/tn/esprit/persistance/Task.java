package tn.esprit.persistance;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity()
@Table(name="Task")
@JsonIgnoreProperties({ "teamMember", "project" })
public class Task implements Serializable {
	@EmbeddedId
	private TaskPk taskpk;
	
	@JsonProperty
	@Enumerated(EnumType.STRING)
	private State state;
	
	@JsonProperty
	@Temporal(TemporalType.DATE)
    private Date DeadLine;
	
	@JsonProperty
	@Temporal(TemporalType.DATE)
    private Date StartDate;
	
	
    @ManyToOne
    @JoinColumn(name="idTeamMember", referencedColumnName="idTeamMember",insertable=false,updatable=false)
    private TeamMember teamMember;
	
	
    @ManyToOne
    @JoinColumn(name="IdProject", referencedColumnName="IdProject",insertable=false,updatable=false)
    private Project project;
   
    
	private static final long serialVersionUID = 2L;
	
	public Task() {
		super();
	}  

	
	public Task(TaskPk taskpk, State state, Date DeadLine, Date StartDate){
		
		this.taskpk=taskpk;
		this.state=state;
		this.DeadLine=DeadLine;
		this.StartDate=StartDate;
		
		
	}
	
	public State GetState() {
		return state;
	}

	public void SetState(State state) {
		this.state = state;
	} 

	public Date GetDeadLine() {
		return DeadLine;
	}

	public void SetDeadLine(Date deadLine) {
		DeadLine = deadLine;
	}

	public Date GetStartDate() {
		return StartDate;
	}

	public void SetStartDate(Date startDate) {
		StartDate = startDate;
	}

    
	
	public Project GetProject() {
		return project;
	}

	public void SetProject(Project project) {
		this.project = project;
	}
	
	public TaskPk getTaskpk() {
		return taskpk;
	}

   
	public void setTaskpk(TaskPk taskpk) {
		this.taskpk = taskpk;
	}

	public TeamMember getTeamMember() {
		return teamMember;
	}

	public void setTeamMember(TeamMember teamMember) {
		this.teamMember = teamMember;
	}


	@Override
	public String toString() {
		return "Task [taskpk=" + taskpk + ", state=" + state + ", DeadLine=" + DeadLine + ", StartDate=" + StartDate+ "]";
	}
	

}
