package tn.esprit.persistance;


import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity()
@Table(name="Category")

public class Category implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IdCategory;
	private String Name;
	private static final long serialVersionUID = 1L;

	@JsonBackReference
	@OneToMany(fetch=FetchType.EAGER,mappedBy="Category")
	private List <Project> projects ;
	
	public Category() {
		super();
	}   
	
	
	public int getIdCategory() {
		return this.IdCategory;
	}

	public void setIdCategory(int IdCategory) {
		this.IdCategory = IdCategory;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}
	public Category(String name) {
		super();
		Name = name;
	}
	public Category(int idCategory, String name) {
		IdCategory = idCategory;
		Name = name;
	}


	public List<Project> getProjects() {
		return projects;
	}


	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
	
	
	
	
   
}