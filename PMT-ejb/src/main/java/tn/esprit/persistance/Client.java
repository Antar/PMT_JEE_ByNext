package tn.esprit.persistance;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name="Client")
public class Client implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer IdClient;
	private String Name;
	private String City;
	private String Country;
	private String Email;
	private Integer Number;
	private Date CreationDate;
	private String website;
	private String Contact;
	private static final long serialVersionUID = 1L;

	public Client() {
		super();
	}   
	public Integer getIdClient() {
		return this.IdClient;
	}

	public void setIdClient(Integer IdClient) {
		this.IdClient = IdClient;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	public String getCity() {
		return this.City;
	}

	public void setCity(String City) {
		this.City = City;
	}   
	public String getCountry() {
		return this.Country;
	}

	public void setCountry(String Country) {
		this.Country = Country;
	}   
	public String getEmail() {
		return this.Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}   
	public Integer getNumber() {
		return this.Number;
	}

	public void setNumber(Integer Number) {
		this.Number = Number;
	}   
	public Date getCreationDate() {
		return this.CreationDate;
	}

	public void setCreationDate(Date CreationDate) {
		this.CreationDate = CreationDate;
	}   
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}   
	public String getContact() {
		return this.Contact;
	}

	public void setContact(String Contact) {
		this.Contact = Contact;
	}
   
}

