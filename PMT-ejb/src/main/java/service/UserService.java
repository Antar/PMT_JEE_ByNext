package service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import interfaces.UserServiceLocal;
import interfaces.UserServiceRemote;
import tn.esprit.persistance.User;

@Stateless
@LocalBean
public class UserService implements  UserServiceLocal ,UserServiceRemote {
	
	
	public UserService() {
		
	}
	@PersistenceContext
	EntityManager em;
	
	@Override
	public boolean addUser(User user) {
		Boolean b = false;
		try {
	em.persist(user);
			b = true;
		} catch (Exception e) {
			System.err.println("problem ...");
		}
		return b;
	
		
	}


	@Override
	public void deleteUser(int id) {
		em.remove(em.find(User.class,id));
		
	}



	@Override
	public User findUserById(int id) {
		
		return em.find(User.class,id);
	}
	
	
    public User findusersByName(String name) {
    	User found = null;
		TypedQuery<User> query = em.createQuery(
				"select u from User u where u.name=:x",User.class);
		query.setParameter("x", name);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO,
					"no user with name=" + name);
		}
		return found;

	}



	@Override
	public void updateUser(User user) {
		em.merge(user);
		
	}
	
	
	@Override
	public User findUserByEmail(String email) {
		return em.find(User.class,email);

}
	
	@Override
	public User findUserByState(int state) {
		return em.find(User.class,state);

}
	
	@Override
	public List<User> findUsernovalidate() {
		return em.createQuery("select u from User u where u.state=:0",User.class)
				.getResultList();
				
				
	}
	@Override
	public List<User> findUservalidate() {
		return em.createQuery("select u from User u where u.state=:1",User.class)
				.getResultList();
				
				
	}
	@Override
	public List<User> findState() {
		return em.createQuery("select u from User u ORDER BY u.state DESC",User.class)
				.getResultList();
				
				
	}
	
	@Override
	public User authentificationUser(String login, String password) {
		User user = null;
		Query query = em
				.createQuery("select u from User u where u.login=:login and u.password=:password");
		query.setParameter("login",login).setParameter("password",password);
		try {
			user= (User) query.getSingleResult();
			return user;
		} catch (Exception exception) {
			return null;
		}
	}
	@Override
	public User authentificationManager(String login, String password) {
		User user = null;
		Query query = em
				.createQuery("select u from User u where u.login=:manager and u.password=:manager");
		query.setParameter("admin",login).setParameter("admin",password);
		try {
			user= (User) query.getSingleResult();
			return user;
		} catch (Exception exception) {
			return null;
		}
	}
	@Override
	public User verifyState(int id) {
		User user = null;
		Query query = em
				.createQuery("select u from User u where u.state=:0 ORDER BY u.state DESC");
		query.setParameter("state",id);
		try {
			user= (User) query.getResultList();
			return user;
		} catch (Exception exception) {
			return null;
		}
	}
    @Override
	public void validateUser(User user ) {
				 		
		user.setState(1);			
    	em.merge(user);			
					
					
					
			
	}
    @Override
   	public void desactivateUser(User user ) {
   				 		
   		user.setState(0);			
       	em.merge(user);			
   					
   					
   					
   			
   	}


	@Override
	public List<User> find() {
		return em.createQuery("select u from User u ",User.class)
				.getResultList();
		
	}

	/*@Override
	public ArrayList<User> getListAccountActivate(int value) {
		Query queryfindusersconfirmed = em.createNamedQuery("findaccountactivate");
		queryfindusersconfirmed.setParameter("value",value);
		return(ArrayList<User>)queryfindusersconfirmed.getResultList();*/
		
		
		
	//}

	/*@Override
	public ArrayList<User> getListAccountDeactivate(int value) {
		Query queryfindusersconfirmed = em.createNamedQuery("findaccountdeactivate");
		queryfindusersconfirmed.setParameter("value",value);
		return (ArrayList<User>)queryfindusersconfirmed.getResultList();
		
	}

	@Override
	public Long getNumberUsersnoconfirmed(int value) {
		
		Query querygetusersnoconfirmed= em.createNamedQuery("getcountusersconfirmed");
		querygetusersnoconfirmed.setParameter("value",value);
		return  (Long) querygetusersnoconfirmed.getSingleResult();
		
	}
	@Override
	public ArrayList<User> getConfirmedUsers() {
		Query querygetconfirmedusers= em.createNamedQuery("getconfirmedusers");	
		return (ArrayList<User>)querygetconfirmedusers.getResultList();
	}*/


	

	
}
