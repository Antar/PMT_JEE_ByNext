package service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.ClientServiceLocal;
import interfaces.ClientServiceRemote;
import tn.esprit.persistance.Client;


/**
 * Session Bean implementation class ClientService
 */
@Stateless
@LocalBean
public class ClientService implements ClientServiceLocal,ClientServiceRemote {

	@PersistenceContext
	EntityManager em ;
    /**
     * Default constructor. 
     */
    public ClientService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public List<Client> GetAllClient() {
		
		return em.createQuery("select c from Client c")
		.getResultList();
		
	}

	@Override
	public void UpdateClient(Client client) {
		em.merge(client);
		
	}

	@Override
	public void DeleteClient(Client client) {
	     em.remove(em.merge(client));		
	}

	@Override
	public void AddClient(Client client) {
		
		em.persist(client);
	}

}
