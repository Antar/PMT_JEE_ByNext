package service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.PostServiceLocal;
import interfaces.PostServiceRemote;
import tn.esprit.persistance.Post;



/**
 * Session Bean implementation class PostService
 */
@Stateless
@LocalBean
public class PostService implements PostServiceRemote, PostServiceLocal {

    /**
     * Default constructor. 
     */
	
	@PersistenceContext
	private EntityManager em;
	
    public PostService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void add(Post post) {
		
		em.persist(post);
	
		
	}

	@Override
	public void delete(Post post) {
		em.remove(em.merge(post));		
	}

	@Override
	public void deleteById(int id) {
		em.remove(em.find(Post.class, id));		
		
	}

	@Override
	public void update(Post post) {
       
		em.merge(post);		
	}



	@Override
	public  List<Post>  findPostsByContent(String Content) {
		return em
				.createQuery("select p from Post p where p.Content=:Content", Post.class)
				.setParameter("Content", Content)
				.getResultList();
	}
	
	@Override
	public List<Post> findAllPosts() {
		return em
		.createQuery("select p from Post p", Post.class)
		.getResultList();
	}

	@Override
	public List<Post> findPostById(int IdPost) {

		return em
				.createQuery("select p from Post p where p.IdPost=:IdPost", Post.class)
				.setParameter("IdPost", IdPost)
				.getResultList();
	}


}
