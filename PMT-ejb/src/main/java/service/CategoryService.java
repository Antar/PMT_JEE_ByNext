package service;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.CategoryServiceLocal;
import interfaces.CategoryServiceRemote;
import tn.esprit.persistance.Category;

/**
 * Session Bean implementation class CategoryService
 */

@Stateless
@LocalBean
public class CategoryService implements CategoryServiceLocal,CategoryServiceRemote{

	
	@PersistenceContext(unitName="PMT-ejb")
	private EntityManager em;
	
   public CategoryService() {
       // TODO Auto-generated constructor stub
   }

	@Override
	public void add(Category category) {
		
		em.persist(category);
	
		
	}

	@Override
	public void delete(Category category) {
		em.remove(em.merge(category));		
	}

	@Override
	public void deleteById(int id) {
		em.remove(em.find(Category.class, id));		
		
	}

	@Override
	public void update(Category category) {
      
		em.merge(category);		
	}



	@Override
	public  List<Category>  findCategoryByName(String Name) {
		return em
				.createQuery("select c from Category c where c.Name=:Name", Category.class)
				.setParameter("Name", Name)
				.getResultList();
	}
	
	@Override
	public List<Category> findAllCategories() {
		return em
		.createQuery("select c from Category c", Category.class)
		.getResultList();
	}

	@Override
	public List<Category> findCategoryById(int IdCategory) {

		return em
				.createQuery("select c from Category c where c.IdCategory=:IdCategory", Category.class)
				.setParameter("IdCategory", IdCategory)
				.getResultList();
	}
	
}
