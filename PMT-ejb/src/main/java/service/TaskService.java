package service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.TaskServiceLocal;
import interfaces.TaskServiceRemote;
import tn.esprit.persistance.State;
import tn.esprit.persistance.Task;
import tn.esprit.persistance.TaskPk;

@Stateless
@LocalBean
public class TaskService implements TaskServiceLocal,TaskServiceRemote {
	
	@PersistenceContext(unitName="PMT-ejb")
	private EntityManager entityManager;
	
	public TaskService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addTask(Task taskToAdd) {
		taskToAdd.SetState(State.ToDo);
		entityManager.persist(taskToAdd);
		
		
	}
	
	@Override
	public void MarkTaskAsDone(Task taskToUpdate){
		taskToUpdate.SetState(State.Done);
		entityManager.persist(taskToUpdate);
	}
	
	@Override
	public void MarkTaskAsDoing(Task taskToUpdate){
		taskToUpdate.SetState(State.Doing);
		entityManager.persist(taskToUpdate);
	}

	
	

	@Override
	public void deleteTask(Task taskToDelete) {
		entityManager.remove(entityManager.merge(taskToDelete));
		
	}

	@Override
	public void deleteTaskById(TaskPk PkTaskToDelete) {
		entityManager.remove(entityManager.find(Task.class, PkTaskToDelete));
		
	}

	@Override
	public void updateTask(Task taskToUpdate) {
		entityManager.merge(taskToUpdate);
		
	}

	@Override
	public Task findTaskById(TaskPk PkTaskToFind) {
		return entityManager.find(Task.class, PkTaskToFind);
	}
 
	@Override
	public List<Task> findAllTasks() {
		return entityManager
				.createQuery("select t from Task t", Task.class)
				.getResultList();
	}
	
	@Override
	public List<Task> AcomplishTasksPerProject(TaskPk PkTaskToShowAccomplishTasks){
		List<Task> AccomplishTasks = null;
		
		String AccomplishTasksQuery="select t from Task t where t.taskpk.idProject=:id and t.state='Done'";
		TypedQuery <Task> query = entityManager.createQuery(AccomplishTasksQuery, Task.class);
		query.setParameter("id", PkTaskToShowAccomplishTasks.getIdProject());
		AccomplishTasks = query.getResultList();
		return AccomplishTasks;
		
	}
	
	@Override
	public List<Task> NotAcomplishTasksPerProject(TaskPk PkTaskToShowAccomplishTasks){
		List<Task> NotAccomplishTasks = null;
		
		String AccomplishTasksQuery="select t from Task t where t.taskpk.idProject=:id and t.state!='Done'";
		TypedQuery <Task> query = entityManager.createQuery(AccomplishTasksQuery, Task.class);
		query.setParameter("id", PkTaskToShowAccomplishTasks.getIdProject());
		NotAccomplishTasks = query.getResultList();
		return NotAccomplishTasks;
		
	}
	
	public String ProjectPercentageOfCompletion(int idProject){
		float Percentage=0;
		String LabelPercentage=null;
		List<Task> AccomplishTasks;
		List<Task> AllTasks;
		int NbrAllTasks=0;
		int NbrAccomplishTasks=0;
		String AccomplishTasksQuery="select t from Task t where t.taskpk.idProject=:id and t.state='Done'";
		TypedQuery <Task> query = entityManager.createQuery(AccomplishTasksQuery, Task.class);
		query.setParameter("id", idProject);
		AccomplishTasks = query.getResultList();
		
		for(Task task:AccomplishTasks){
	    	 NbrAccomplishTasks ++;
	    	 
	     }
		
		String AllTasksQuery="select t from Task t where t.taskpk.idProject=:id";
		TypedQuery <Task> query2 = entityManager.createQuery(AllTasksQuery, Task.class);
		query2.setParameter("id", idProject);
		AllTasks = query2.getResultList();
		
		for(Task task:AllTasks){
	    	 NbrAllTasks ++;
	    	 
	     }
		
		Percentage = (100*NbrAccomplishTasks)/NbrAllTasks;
		return LabelPercentage = "The Percentage of Completion of this Project is: "+Percentage+"%";
	}
	
	@Override
	public String testStatutOfTask(TaskPk PkTaskToShowAccomplishTasks) {
		Date end = null ;
		Date Start = null;
		int stillhaveDays  = 0 ;
		List <Task>  TaskDate ; 
		String  query1 =  "select t from Task t where t.taskpk.idProject=:id and t.taskpk.idTeamMember=:idTm and t.taskpk.Description=:des" ;
		
			TaskDate = 	entityManager.createQuery(query1, Task.class)
				.setParameter("id", PkTaskToShowAccomplishTasks.getIdProject())
				.setParameter("idTm", PkTaskToShowAccomplishTasks.getIdTeamMember())
				.setParameter("des", PkTaskToShowAccomplishTasks.GetDescription())
				.getResultList();
			
			for(Task task:TaskDate)
			{
				
				Start = task.GetStartDate();
				end = task.GetDeadLine();
			}
			
			Calendar now = Calendar.getInstance();
			
			if(now.getTime().compareTo(end)>0){
					
		return "you have passed the DeadLine for this task ";
			}
		else 
			stillhaveDays= (int)( (end.getTime() - now.getTimeInMillis()) / (1000 * 60 * 60 * 24) ) ;
			
			return "you have "+stillhaveDays+" days before the DeadLine for this task";
	}

	
	

}
