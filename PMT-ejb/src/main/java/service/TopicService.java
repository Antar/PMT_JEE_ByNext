package service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.TopicServiceLocal;
import interfaces.TopicServiceRemote;
import tn.esprit.persistance.Topic;
import javax.ejb.Stateless;
/**
 * Session Bean implementation class TopicService
 */
@Stateless
@LocalBean
public class TopicService implements TopicServiceRemote, TopicServiceLocal {

    /**
     * Default constructor. 
     */
	
	@PersistenceContext
	private EntityManager em;
	
    public TopicService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void add(Topic topic) {
		
		em.persist(topic);
	
		
	}

	@Override
	public void delete(Topic topic) {
		em.remove(em.merge(topic));		
	}

	@Override
	public void deleteById(int id) {
		em.remove(em.find(Topic.class, id));		
		
	}

	@Override
	public void update(Topic topic) {
       
		em.merge(topic);		
	}



	@Override
	public  List<Topic>  findTopicsByTitle(String Title) {
		return em
				.createQuery("select t from Topic t where t.Topic=:Topic", Topic.class)
				.setParameter("Title", Title)
				.getResultList();
	}
	
	@Override
	public List<Topic> findAllTopics() {
		return em
		.createQuery("select t from Topic t", Topic.class)
		.getResultList();
	}

	@Override
	public List<Topic> findTopicById(int IdTopic) {

		return em
				.createQuery("select t from Topic t where t.IdPost=:IdPost", Topic.class)
				.setParameter("IdPost", IdTopic)
				.getResultList();
	}

}
