package service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.ProjectServiceLocal;
import interfaces.ProjectServiceRemote;
import tn.esprit.persistance.Project;

/**
 * Session Bean implementation class ProjectService
 */

@Stateless
@LocalBean
public class ProjectService implements ProjectServiceLocal,ProjectServiceRemote{
	
	@PersistenceContext(unitName="PMT-ejb")
	private EntityManager em;
	
    public ProjectService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void add(Project project) {
		em.persist(project);
	}

	@Override
	public void delete(Project project) {

		em.remove(em.merge(project));
	}

	@Override
	public void deleteById(int id) {

		em.remove(em.find(Project.class, id));		

	}

	@Override
	public void update(Project project) {

		em.merge(project);
	}

	@Override
	public Project findProjectById(int id) {
		return em.find(Project.class, id);
	}

	@Override
	public List<Project> findProjectByName(String Name) {
		return em
				.createQuery("select p from Project p where p.Name=:Name", Project.class)
				.setParameter("Name", Name)
				.getResultList();
	}

	@Override
	public List<Project> findAllProjects() {
		return em
				.createQuery("select p from Project p", Project.class)
				.getResultList();
	}

}
