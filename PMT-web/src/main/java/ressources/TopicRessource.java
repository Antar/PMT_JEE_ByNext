package ressources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import interfaces.TopicServiceLocal;
import tn.esprit.persistance.Topic;

@Path("/topic")
@RequestScoped
public class TopicRessource {

	@Inject
	TopicServiceLocal myejb;
	
	@POST
	@Produces("application/xml")
	public Response add(Topic topic)
	{
		myejb.add(topic);
		return Response.ok("Topic successfully Added").status(Status.OK).build();

       
	}
	
	@PUT
	@Consumes("application/json")
    public  Response update(Topic topic)
	{
			 myejb.update(topic);
			 return Response.ok("updated").status(Status.OK).build();
			 
	}
	

	
	@DELETE
	@Path("/{id}")
	@Consumes("application/json")
	public Response deleteByid (@PathParam("id") Integer id) {

		 myejb.deleteById(id);

			return Response.ok("Topic deleted").status(Status.OK).build();

	}

	
	
	
	@GET
	@Path("/{Title}")
	@Produces("application/json")
     public Response findByName(@PathParam("Title") String Title ) {
		
		
		List<Topic>  found = myejb.findTopicsByTitle(Title);
		
		if (found==null)
			return Response.status(Status.NOT_FOUND).entity("Topic Not Found").build();
		else
			return Response.ok(found).build();
		
	}
	
	
	

	

	@GET
	@Produces("application/json")
	public Response getAllPosts() {
		
		List<Topic> topics = myejb.findAllTopics(); ;
		
		if (topics==null)
			return Response.status(Status.NOT_FOUND).entity("Topic Not Found").build();
		else
			return Response.ok(topics).build();
	}
	
	
}

