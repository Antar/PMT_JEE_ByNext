package ressources;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import interfaces.UserServiceLocal;
import tn.esprit.persistance.User;

@Path("user")
@RequestScoped
public class UserRessource {
	
	
	
	@EJB
	UserServiceLocal userService;
	static List<User> users = new ArrayList<User>();

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON) 
	@Produces(MediaType.TEXT_PLAIN)
	
	public Response createUser(User user)
	{
		userService.addUser(user);
		return Response.ok(user).build();
		   
		
	}
    
	   @DELETE
	   @Path("{id}")
	   @Consumes("application/json")
	   public void deletebyID(@PathParam("id")int id)
		{
		
		   userService.deleteUser(id);
		   
			
		}
	   
	   
	    @GET
	    @Produces("application/json")
		@Path("{id}")
		public User findUserbyID(@PathParam("id")int id)
		{
		return	userService.findUserById(id);
			
		}
	    
	    
	    @GET
	    @Produces("application/json")
		@Path("name/{name}")
		public User findUserbyName(@PathParam("name")String name)
		{
		return	userService.findusersByName(name);
			
		}
	    
	    
	    
	    
	    @GET
	    @Produces("application/json")
		@Path("state/{state}")
		public User findUserbyState(@PathParam("state")int state)
		{
		return	userService.findUserByState(state);
			
		}
	    
	    @GET
		@Produces("application/json")
		
		public List<User> retrieveUsers()
		{
			return userService.find();
		
		}
	    @GET
		@Produces("application/json")
	    @Path("/state")
		public List<User> findUsers()
		{
			return userService.findState();
		
		}
	    @PUT
	    @Consumes ("application/json")
	    public Response  UpdateUser(User user)
	    {
	 	   userService.updateUser(user);
	 	  return Response.ok(user).build();
	    }
	 	   
	    
	    

	    @GET
	    @Produces("application/json")
		@Path("auth/{login}/{password}")
		public User authentificate(@PathParam("login")String login,@PathParam("password")String password)
		{
		return	userService.authentificationUser(login,password);
			
		}
	    @GET
	    @Produces("application/json")
		@Path("manager/{manager}/{manager}")
		public User authentificateAdmin(@PathParam("manager")String login,@PathParam("manager")String password)
		{
	return	userService.authentificationManager(login,password);
			
		}
	    @GET
	    @Produces("application/json")
		@Path("{state}")
		public User verifyState(@PathParam("state")int state)
		{
	return	userService.verifyState(state);
			
		}
	    @PUT
	    @Produces("application/json")
		@Path("validate")
		public User validateUser(User user)
		{
	userService.validateUser(user);
		return user;		
		}
	    @PUT
	    @Produces("application/json")
		@Path("desactivate")
		public User desactivateUser(User user)
		{
	userService.desactivateUser(user);
		return user;		
		}
	    
	    
}

