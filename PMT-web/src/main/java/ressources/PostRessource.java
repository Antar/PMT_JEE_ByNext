package ressources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import interfaces.PostServiceLocal;
import tn.esprit.persistance.Post;

@Path("/post")
@RequestScoped
public class PostRessource {

	@Inject
	PostServiceLocal myejb;
	
	@POST
	@Produces("application/xml")
	public Response add(Post post)
	{
		myejb.add(post);
		return Response.ok("Post successfully Added").status(Status.OK).build();

       
	}
	
	@PUT
	@Consumes("application/json")
    public  Response update(Post post)
	{
			 myejb.update(post);
			 return Response.ok("updated").status(Status.OK).build();
			 
	}
	

	
	@DELETE
	@Path("/{id}")
	@Consumes("application/json")
	public Response deleteByid (@PathParam("id") Integer id) {

		 myejb.deleteById(id);

			return Response.ok("Post deleted").status(Status.OK).build();

	}

	
	
	
	@GET
	@Path("/{Content}")
	@Produces("application/json")
     public Response findByName(@PathParam("Content") String Content ) {
		
		
		List<Post>  found = myejb.findPostsByContent(Content);
		
		if (found==null)
			return Response.status(Status.NOT_FOUND).entity("Post Not Found").build();
		else
			return Response.ok(found).build();
		
	}
	
	
	

	

	@GET
	@Produces("application/json")
	public Response getAllPosts() {
		
		List<Post> posts = myejb.findAllPosts(); ;
		
		if (posts==null)
			return Response.status(Status.NOT_FOUND).entity("Post Not Found").build();
		else
			return Response.ok(posts).build();
	}
	
	
}

