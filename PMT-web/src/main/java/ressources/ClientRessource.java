package ressources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import interfaces.ClientServiceLocal;
import tn.esprit.persistance.Client;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;




@Path("/client")
@RequestScoped
public class ClientRessource {
	@Inject
	ClientServiceLocal myejb;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	public void add(Client client)
	{
		myejb.AddClient(client);
	}
	@PUT
    @Path("/update")
	@Consumes("application/json")
    public void update(Client client)
	{
			 myejb.UpdateClient(client);
	}
	@DELETE
	@Path("/delete")
	@Consumes("application/json")
	public void delete(Client client) {

		 myejb.DeleteClient(client);

	}
	@GET
	@Path("/findAll")
	@Produces("application/json")
	public List<Client> getAll() {
		
		return myejb.GetAllClient();
		
	}
}
