package ressources;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import interfaces.TaskServiceLocal;
import tn.esprit.persistance.Task;
import tn.esprit.persistance.TaskPk;

@Path("/tasks")
@RequestScoped
public class TaskRessource {
	@Inject
	TaskServiceLocal TaskEjb;
	
	
	
	@GET
	//@Path("/find")
	@Produces(MediaType.APPLICATION_JSON)
	public Response find() {

		List<Task> AllTasks =TaskEjb.findAllTasks();
		
		if (AllTasks==null)
			return Response.status(Status.NOT_FOUND).entity("No Tasks Found").build();
		else
			return Response.ok(AllTasks).build();

	}
	
	@GET
	@Path(value="/{id}/{idT}/{desc}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response DeadlineVerification(@PathParam(value="id")int idProject,@PathParam(value="idT")int idTeamMember,@PathParam(value="desc")String Description ) {
		
        TaskPk taskpk = new TaskPk(idTeamMember,idProject,Description);
        
		String State=TaskEjb.testStatutOfTask(taskpk);
		
		if (State==null)
			return Response.status(Status.NOT_FOUND).entity("No Tasks Found").build();
		else
			return Response.ok(State).build();

	}
	
	
	
	@GET
	@Path(value="/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response ProjectPercentageOfCompletion(@PathParam(value="id")int idProject ) {
  
		String Percentage=TaskEjb.ProjectPercentageOfCompletion(idProject);
		
		if (Percentage==null)
			return Response.status(Status.NOT_FOUND).entity("No Tasks Fouidd").build();
		else
			return Response.ok(Percentage).build();

	}
	
	
	
	
//	@GET
//	@Path("/find/{id}")
//	@Produces("application/json")
//     public Task find(@PathParam("id") Integer idTaskToFind) {
//		return TaskEjb.findTaskById(idTaskToFind);
//	}
//	
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	public void add(Task taskToAdd) {
		TaskEjb.addTask(taskToAdd);
	}
	
	@DELETE
	@Consumes("application/json")
	public void delete(Task taskToDelete) {

		 TaskEjb.deleteTask(taskToDelete);

	}
	

}
